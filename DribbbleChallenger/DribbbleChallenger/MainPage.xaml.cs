﻿using DribbbleChallenger.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace DribbbleChallenger
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        #region Declare

        //private List<ShotsModel> _shotProperty;
        //public List<ShotsModel> ShotProperty
        //{
        //    get
        //    {
        //        return _shotProperty;
        //    }
        //    set
        //    {
        //        if (value != _shotProperty)
        //        {
        //            _shotProperty = value;
        //            NotifyPropertyChanged("ShotProperty");
        //        }
        //    }
        //}

        //private List<ShotsModel> _shotsListProperty;
        //public List<ShotsModel> ShotsListProperty
        //{
        //    get
        //    {
        //        return _shotsListProperty;
        //    }
        //    set
        //    {
        //        if (value != _shotsListProperty)
        //        {
        //            _shotsListProperty = value;
        //            NotifyPropertyChanged("ShotsListProperty");
        //        }
        //    }
        //}

        //public event PropertyChangedEventHandler PropertyChanged;
        //private void NotifyPropertyChanged(String propertyName)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;
        //    if (null != handler)
        //    {
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}



        public string Views_Count
        {
            get { return (string)GetValue(Views_CountProperty); }
            set { SetValue(Views_CountProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Views_Count.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Views_CountProperty =
            DependencyProperty.Register("Views_Count", typeof(string), typeof(MainPage), null);

        

        #endregion

        #region Public

        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void SetValueDp(DependencyProperty property, object value,
            [System.Runtime.CompilerServices.CallerMemberName] String p = null)
        {
            SetValue(property, value);
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(p));

        }

        #endregion

        #region Private
        
        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            PopularShotsModel shots = e.Parameter as PopularShotsModel;
            ShotsList.ItemsSource = shots.Shots;
        }

        private void ShotsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var shot = ShotsList.SelectedItem as ShotsModel;
            //ShotsModel shot = _shotsListProperty.Where(x => x.Id.Equals(e.))
            Frame.Navigate(typeof(DetailPage), shot);
        }

        #endregion
    }
}
