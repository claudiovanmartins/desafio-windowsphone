﻿using DribbbleChallenger.Common;
using DribbbleChallenger.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace DribbbleChallenger
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DetailPage : Page, INotifyPropertyChanged
    {
        #region Declare
        
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();


        public string Image_400_Url
        {
            get { return (string)GetValue(Image_400_UrlProperty); }
            set { SetValue(Image_400_UrlProperty, value); }
        }

        public static readonly DependencyProperty Image_400_UrlProperty =
            DependencyProperty.Register("Image_400_Url", typeof(string), typeof(DetailPage), new PropertyMetadata(0));

        public string Image_Teaser_Url
        {
            get { return (string)GetValue(Image_Teaser_UrlProperty); }
            set { SetValue(Image_Teaser_UrlProperty, value); }
        }

        public static readonly DependencyProperty Image_Teaser_UrlProperty =
            DependencyProperty.Register("Image_Teaser_Url", typeof(string), typeof(DetailPage), new PropertyMetadata(0));

        public string NameShot
        {
            get { return (string)GetValue(NameShotProperty); }
            set { SetValueDp(NameShotProperty, value); }
        }

        public static readonly DependencyProperty NameShotProperty =
            DependencyProperty.Register("NameShot", typeof(string), typeof(DetailPage), null);

        public string DescriptionShot
        {
            get { return (string)GetValue(DescriptionShotProperty); }
            set { SetValueDp(DescriptionShotProperty, value); }
        }

        public static readonly DependencyProperty DescriptionShotProperty =
            DependencyProperty.Register("DescriptionShot", typeof(string), typeof(DetailPage), null);



        public string CreatedAt
        {
            get { return (string)GetValue(CreatedAtProperty); }
            set { SetValue(CreatedAtProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CreatedAt.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CreatedAtProperty =
            DependencyProperty.Register("CreatedAt", typeof(string), typeof(DetailPage),null);



        public string Likes_Count
        {
            get { return (string)GetValue(Likes_CountProperty); }
            set { SetValue(Likes_CountProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Likes.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Likes_CountProperty =
            DependencyProperty.Register("Likes", typeof(string), typeof(DetailPage), null);



        public string Views_Count
        {
            get { return (string)GetValue(Views_CountProperty); }
            set { SetValue(Views_CountProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Views_Count.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Views_CountProperty =
            DependencyProperty.Register("Views_Count", typeof(string), typeof(DetailPage),null );


        #endregion


        public DetailPage()
        {
            this.InitializeComponent();
            (this.Content as FrameworkElement).DataContext = this;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;


        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            
            ShotsModel shot = e.NavigationParameter as ShotsModel;
            if (shot != null)
            {
                NameShot = shot.Title;
                Image_Teaser_Url = shot.Image_Teaser_Url;
                Image_400_Url = shot.Image_400_Url;
                DescriptionShot = string.IsNullOrEmpty(shot.Description) ? shot.Title : WebUtility.HtmlDecode(shot.Description.ToString());
                CreatedAt = shot.Created_At.ToString();
                Likes_Count = shot.Likes_Count.ToString();
                Views_Count = shot.Views_Count.ToString();

            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        void SetValueDp(DependencyProperty property, object value,
            [System.Runtime.CompilerServices.CallerMemberName] String p = null)
        {
            SetValue(property, value);
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(p));

        }
        //private void NotifyPropertyChanged(String propertyName)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;
        //    if (null != handler)
        //    {
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}
    }
}
