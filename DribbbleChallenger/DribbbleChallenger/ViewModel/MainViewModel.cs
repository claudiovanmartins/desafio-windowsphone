﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DribbbleChallenger.Model;
using DribbbleChallenger.DataSource;

namespace DribbbleChallenger.ViewModel
{
    public class MainViewModel
    {
        #region Declare



        #endregion

        #region Private

        

        #endregion

        #region Public

        public async Task<PopularShotsModel> LoadPopularShots()
        {
            IAPIDataSource IAPI;
            IAPI = new APIDataSource();

            IShotsDataSource shots = new ShotsDataSource(IAPI);
            var allShots = await shots.GetPopularShots();

            return allShots;

        }

        public async Task<List<ShotsModel>> LoadData()
        {
            IAPIDataSource IAPI;
            IAPI = new APIDataSource();

            IShotsDataSource shots = new ShotsDataSource(IAPI);
            var allShots = await shots.GetPopularShots();

            List<ShotsModel> listOfShots = allShots.Shots;

            return listOfShots;

        }

        #endregion
    }
}
