﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DribbbleChallenger.Model
{
    public class    PlayerModel
    {
        public string Avatar_Url { get; set; }
        public int Comments_Count { get; set; }
        public int Comments_Received_Count { get; set; }
        public DateTime Created_At { get; set; }
        public int Drafted_By_Player_Id { get; set; }
        public int Draftees_Count { get; set; }
        public int Followers_Count { get; set; }
        public int Following_Count { get; set; }
        public int Id { get; set; }
        public int Likes_Count { get; set; }
        public int Likes_Received_Count { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        public int Rebounds_Count { get; set; }
        public int Rebounds_Received_Count { get; set; }
        public int Shots_Count { get; set; }
        public string Twitter_Screen_Name { get; set; }
        public string Url { get; set; }
        public string Username { get; set; }
        public string Website_Url { get; set; }

    }
}
