﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DribbbleChallenger.Model
{
    public class ShotsModel
    {
        public int Comments_Count { get; set; }
        public DateTime Created_At { get; set; }
        public string Description { get; set; }
        public int Height { get; set; }
        public int Id { get; set; }
        public string Image_400_Url { get; set; }
        public string Image_Teaser_Url { get; set; }
        public string Image_Url { get; set; }
        public int Likes_Count { get; set; }
        public List<PlayerModel> Players { get; set; }
        public int? Rebound_Source_Id { get; set; }
        public int Rebounds_Count { get; set; }
        public string Short_Url { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public int Views_Count { get; set; }
        public int Width { get; set; }
    }
}
