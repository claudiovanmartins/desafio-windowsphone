﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DribbbleChallenger.Model
{
    public class PopularShotsModel
    {
        public int Page { get; set; }
        public int Pages { get; set; }
        public int Per_Page { get; set; }
        public List<ShotsModel> Shots { get; set; }
        public int Total { get; set; }
    }
}
