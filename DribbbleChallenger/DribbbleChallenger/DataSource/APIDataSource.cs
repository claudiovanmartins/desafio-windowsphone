﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using DribbbleChallenger.Model;
using Newtonsoft.Json;


namespace DribbbleChallenger.DataSource
{
    public class APIDataSource : DataSourceBase, IAPIDataSource
    {

        #region Declare

        //const string API_URL = "http://api.dribbble.com/shots/";

        #endregion

        #region Private

        private static async Task<string> GetJsonAsync(string url)
        {
            using (var _client = new HttpClient())
            {
                _client.BaseAddress = new Uri(ApiUriSettingDefault);
                _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                using (var response = await _client.GetAsync(url))
                {
                    response.EnsureSuccessStatusCode();
                    return await response.Content.ReadAsStringAsync();
                }
            }
        }

        private static async Task<string> PostJsonAsync(string url)
        {
            using (var _client = new HttpClient())
            {
                _client.BaseAddress = new Uri(ApiUriSettingDefault);
                _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                using (var response = await _client.PostAsync(url, null))
                {
                    response.EnsureSuccessStatusCode();
                    return await response.Content.ReadAsStringAsync();
                }
            }
        }

        private static async Task<T> GetObject<T>(string url)
            where T : class
        {
            var json = await GetJsonAsync(url);
            var jsonReturn = JsonConvert.DeserializeObject<T>(json);
            return jsonReturn;
        }

        private static async Task<T> GetObjectAsync<T>(string url)
         where T : class
        {
            var json = await GetJsonAsyncWithRetries(url);

            return JsonConvert.DeserializeObject<T>(json);
        }

        private static async Task<string> GetJsonAsyncWithRetries(string url)
        {
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    return await GetJsonAsync(url);
                }
                catch { }
                await Task.Delay(i * 250);
            }
            return null;
        }

        #endregion

        #region Public

        public async Task<PopularShotsModel> PopularShotsAsync()
        {
            string url = string.Format("/shots/popular");
            return await GetObjectAsync<PopularShotsModel>(url);
            //return await GetObject<PopularShotsModel>(url);
        }

        public async Task<ShotsModel> ShotDetailAsync(int shotID)
        {
            string url = string.Format("/shots/{0}", shotID);
            return await GetObjectAsync<ShotsModel>(url);
        }

        public async Task<PopularShotsModel> PopularShotsPaginationAsync(int page)
        {
            string url = string.Format("/shots/popular?page={0}", page);
            return await GetObjectAsync<PopularShotsModel>(url);
        }

        #endregion





    }
}
