﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DribbbleChallenger.Common;

namespace DribbbleChallenger.DataSource
{
    public class DataSourceBase
    {
        #region Declare

        private const string _apiUriSettingKeyName = "ApiUriSetting";
        private const string _apiUriSettingDefault = "http://api.dribbble.com/";

        internal static string ApiUriSettingKeyName { get; set; }
        internal static string ApiUriSettingDefault { get; set; }

        #endregion

        public DataSourceBase()
        {
            AppSettings settings = new AppSettings();

            if (settings.AddOrUpdateValue(_apiUriSettingKeyName, _apiUriSettingDefault))
            {
                ApiUriSettingKeyName = _apiUriSettingKeyName;
                ApiUriSettingDefault = _apiUriSettingDefault;
            }
            else
            {
                ApiUriSettingKeyName = _apiUriSettingKeyName;
                ApiUriSettingDefault = settings.GetValueOrDefault(_apiUriSettingKeyName, _apiUriSettingDefault);
            }
        }
    }
}

