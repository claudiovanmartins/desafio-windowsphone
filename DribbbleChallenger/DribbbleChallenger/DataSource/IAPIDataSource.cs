﻿using DribbbleChallenger.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DribbbleChallenger.DataSource
{
    public interface IAPIDataSource
    {
        Task<PopularShotsModel> PopularShotsAsync();

        Task<PopularShotsModel> PopularShotsPaginationAsync(int page);

        Task<ShotsModel> ShotDetailAsync(int shotID);

    }
}
