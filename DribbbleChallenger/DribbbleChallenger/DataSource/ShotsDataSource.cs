﻿using DribbbleChallenger.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DribbbleChallenger.DataSource
{
    public class ShotsDataSource : IShotsDataSource
    {

        IAPIDataSource IAPIData;

        public ShotsDataSource(IAPIDataSource IAPI)
        {
            IAPIData = IAPI;
        }

        public async Task<PopularShotsModel> GetPopularShots()
        {
            return await IAPIData.PopularShotsAsync();
        }

        public async Task<PopularShotsModel> GetPopularShotsPagination(int page)
        {
            return await IAPIData.PopularShotsPaginationAsync(page);
        }

        public async Task<ShotsModel> GetShotDetail(int shotID)
        {
            return await IAPIData.ShotDetailAsync(shotID);
        }
    }
}
