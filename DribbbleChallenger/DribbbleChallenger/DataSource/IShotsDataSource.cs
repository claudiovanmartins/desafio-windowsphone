﻿using DribbbleChallenger.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DribbbleChallenger.DataSource
{
    public interface IShotsDataSource
    {
        Task<PopularShotsModel> GetPopularShots();

        Task<PopularShotsModel> GetPopularShotsPagination(int page);

        Task<ShotsModel> GetShotDetail(int shotID);

    }
}
