﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace DribbbleChallenger.Common
{
    public class AppSettings
    {
    
        public bool AddOrUpdateValue(string Key, Object value)
        {
            bool valueChanged = false;

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey(Key))
            {
                if (ApplicationData.Current.LocalSettings.Values[Key] != value)
                {
                    ApplicationData.Current.LocalSettings.Values[Key] = value;
                    valueChanged = true;
                }
            }
            else
            {
                ApplicationData.Current.LocalSettings.Values.Add(Key, value);
                valueChanged = true;
            }

            return valueChanged;
        }

        public T GetValueOrDefault<T>(string Key, T defaultValue)
        {
            T value;

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey(Key))
            {
                value = (T)ApplicationData.Current.LocalSettings.Values[Key];
            }
            else
            {
                value = defaultValue;
            }

            return value;
        }
    }
}
